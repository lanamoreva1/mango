import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Notification from '../views/Notification.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/notification',
      name: 'notification',
      // query: 'id',
      component: Notification
    },
    // {
    //   path: '/notarget',
    //   name: 'notarget',
    //   component: NoTargetView
    // }
  ]
})

export default router
