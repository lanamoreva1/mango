import './assets/main.scss'

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { setupCalendar, Calendar, DatePicker } from 'v-calendar';
import 'v-calendar/style.css';
import axios from 'axios'
import VueAxios from 'vue-axios'

const clickOutside = {
  beforeMount: (el, binding) => {
    el.clickOutsideEvent = (event) => {
      // here I check that click was outside the el and his children
      if (!(el == event.target || el.contains(event.target))) {
        // and if it did, call method provided in attribute value
        binding.value()
      }
    }
    document.addEventListener('click', el.clickOutsideEvent)
  },
  unmounted: (el) => {
    document.removeEventListener('click', el.clickOutsideEvent)
  }
}
// v-click-outside="openWarning"

const app = createApp(App)
app.component('VCalendar', Calendar)
app.component('VDatePicker', DatePicker)
app.directive('click-outside', clickOutside)
app.use(router, setupCalendar)
app.use(VueAxios, axios)
app.mount('#app')

const API_URL = 'http://192.168.2.174:59508'
export const getListName = () => {
  return axios.get(API_URL + '/getEmployees')
}
export const getAudio = (data) => {
  return axios.get(`${API_URL}/getAudio/${data}`)
}
export const getRecords = (data) => {
  return axios.post(API_URL + '/getRecords', data)
}
export const updateRecord = (data) => {
  return axios.put(`${API_URL}/updateRecord`, data)
}
export const getSets = () => {
  return axios.get(`${API_URL}/getSets`)
}
export const changeActiveSet  = (data) => {
  return axios.put(`${API_URL}/changeActiveSet`, data)
}
export const postSet  = (data) => {
  return axios.post(API_URL + '/postSet', data)
}


export function noScroll(isPopup) {
  const div = document.createElement('div')
  div.style.overflowY = 'scroll'
  div.style.width = '50px'
  div.style.height = '50px'
  document.body.append(div)
  const scrollWidth = div.offsetWidth - div.clientWidth
  div.remove()
  document.body.style.overflow = isPopup ? 'hidden' : 'auto'
  document.body.style.marginRight = isPopup ? scrollWidth + 'px' : 0
}

export const headerTable = [
    { id: 1, title: '', value: 'id' },
    { id: 2, title: '', value: 'photo' },
    { id: 4, title: 'Дата звонка', value: 'date' },
    { id: 3, title: 'ФИО', value: 'name' },
    { id: 8, title: 'Доп.инфа', value: 'info' },
  ]
// export const user = {
//     // id: 1,
//     photo: `@/assets/img/1.jpg`,
//     name: 'Морева Светлана Андреевна',
//     date: '02.04.1995',
//     city: 1,
//     address: 'Кирова, 11',
//     tel: '9999999999',
//     info: 'инфы нет',
//     gender: 'женский',
//     active: true
//   }
export const callTypes = [
  { id: 1, name: 'Целевой', value: true},
  { id: 2, name: 'Не целевой', value: false},
]
// export const nameManagers = [
//     { id: 1, title: 'Иванов Иван Иванович'},
//     { id: 2, title: 'Петров Петр Петрович'},
//     { id: 3, title: 'Петров Петр Петрович11'},
//     { id: 4, title: 'Gfdfdf Fjndjhjd Sjijijs'},
//     { id: 5, title: 'Петров Петр Петрович22'},
//     { id: 6, title: 'Gfdfdf Fjndjhjd Sjijijs'},
//     { id: 7, title: 'Gfdfdf Fjndjhjd Sjijijs'},
//     { id: 8, title: 'Gfdfdf Fjndjhjd Sjijijs'},
//     { id: 9, title: 'Gfdfdf Fjndjhjd Sjijijs'},
//     { id: 10,title: 'Gfdfdf Fjndjhjd Sjijijs'},
//   ]
export const managers = [
  {
    id: 1,
    target: true,
    title: '',
    changeTarget: null,
    opened: false,
    name: 'Иванов Иван Иванович',
    date: '11.12.23',
    avatar: '',
    audio: 'http://www.jplayer.org/audio/mp3/Miaow-02-Hidden.mp3',
    text: 'Fugiat voluptate do irure ea Lorem mollit pariatur duis proident voluptate mollit ut Lorem. Eiusmod occaecat Lorem officia veniam duis eiusmod quis aute elit occaecat consectetur minim id. Et Lorem et enim id nisi dolore. Incididunt cupidatat Lorem nulla esse exercitation non anim ipsum laborum.'
  },
  {
    id: 2,
    target: false,
    opened: false,
    changeTarget: null,
    title: '',
    name: 'Петров Петр Петрович',
    date: '14.12.23',
    avatar: '/src/assets/img/avatarrrr.png',
    audio: 'http://www.jplayer.org/audio/mp3/Miaow-02-Hidden.mp3',
    text: 'Qui anim voluptate consequat commodo exercitation sunt mollit reprehenderit exercitation amet proident. Duis nisi esse deserunt qui consequat deserunt. Esse quis ex et et velit et ipsum velit sit dolor non magna dolore commodo. Laboris in do sunt nulla commodo minim dolor sint occaecat veniam commodo. Est est sit occaecat aute ipsum nostrud dolore.'
  },
  {
    id: 3,
    target: false,
    opened: false,
    changeTarget: null,
    title: '',
    name: 'Петров Петр Петрович22',
    date: '17.12.23',
    avatar: '/src/assets/img/avatarrrr.png',
    audio: 'http://www.jplayer.org/audio/mp3/Miaow-02-Hidden.mp3',
    text: 'Qui anim voluptate consequat commodo exercitation sunt mollit reprehenderit exercitation amet proident. Duis nisi esse deserunt qui consequat deserunt. Esse quis ex et et velit et ipsum velit sit dolor non magna dolore commodo. Laboris in do sunt nulla commodo minim dolor sint occaecat veniam commodo. Est est sit occaecat aute ipsum nostrud dolore.'
  },
  {
    id: 4,
    target: false,
    changeTarget: null,
    opened: false,
    title: '',
    name: 'Петров Петр Петрович11',
    date: '05.12.23',
    avatar: '/src/assets/img/avatarrrr.png',
    audio: 'http://www.jplayer.org/audio/mp3/Miaow-02-Hidden.mp3',
    text: 'Qui anim voluptate consequat commodo exercitation sunt mollit reprehenderit exercitation amet proident. Duis nisi esse deserunt qui consequat deserunt. Esse quis ex et et velit et ipsum velit sit dolor non magna dolore commodo. Laboris in do sunt nulla commodo minim dolor sint occaecat veniam commodo. Est est sit occaecat aute ipsum nostrud dolore.'
  },
  {
    id: 5,
    target: true,
    changeTarget: null,
    opened: false,
    title: '',
    name: 'Петров Петр Петрович',
    date: '19.12.23',
    avatar: '/src/assets/img/avatarrrr.png',
    audio: 'http://www.jplayer.org/audio/mp3/Miaow-02-Hidden.mp3',
    text: `Qui anim voluptate consequat commodo exercitation sunt mollit reprehenderit exercitation amet proident. Duis nisi esse deserunt qui consequat deserunt. Esse quis ex et et velit et ipsum velit sit dolor non magna dolore commodo. Laboris in do sunt nulla commodo minim dolor sint occaecat veniam commodo. Est est sit occaecat aute ipsum nostrud dolore.
    Qui anim voluptate consequat commodo exercitation sunt mollit reprehenderit exercitation amet proident. Duis nisi esse deserunt qui consequat deserunt. Esse quis ex et et velit et ipsum velit sit dolor non magna dolore commodo. Laboris in do sunt nulla commodo minim dolor sint occaecat veniam commodo. Est est sit occaecat aute ipsum nostrud dolore.
    Qui anim voluptate consequat commodo exercitation sunt mollit reprehenderit exercitation amet proident. Duis nisi esse deserunt qui consequat deserunt. Esse quis ex et et velit et ipsum velit sit dolor non magna dolore commodo. Laboris in do sunt nulla commodo minim dolor sint occaecat veniam commodo. Est est sit occaecat aute ipsum nostrud dolore.
    Qui anim voluptate consequat commodo exercitation sunt mollit reprehenderit exercitation amet proident. Duis nisi esse deserunt qui consequat deserunt. Esse quis ex et et velit et ipsum velit sit dolor non magna dolore commodo. Laboris in do sunt nulla commodo minim dolor sint occaecat veniam commodo. Est est sit occaecat aute ipsum nostrud dolore.
    `
  },
]
